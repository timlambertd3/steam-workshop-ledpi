from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image
from ledpi.globals.global_vars import order_of_addition
import time


def display(image_dict, name):
    """
    Displays an image onto the pi led board.
    :param image_dict: Dict representing an image.
    :return: Success or failure.
    """

    if name not in order_of_addition:
        order_of_addition.append(name)
        print('Team [{0}] got their animal on the board!'.format(name))

    image = Image.open(image_dict['address'])

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.rows = 32
    options.cols = 64
    options.chain_length = 1
    options.parallel = 1
    options.hardware_mapping = 'adafruit-hat'
    options.pwm_lsb_nanoseconds = 50
    options.drop_privileges = False

    matrix = RGBMatrix(options=options)

    # Make image fit our screen.
    image.thumbnail((matrix.width, matrix.height), Image.ANTIALIAS)

    matrix.SetImage(image.convert('RGB'), image_dict['offset'])

    time.sleep(5)

    matrix.Clear()

    del matrix
