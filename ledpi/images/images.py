from ledpi.modles.image import Image

"""
Contains information about the address and description of an animal image.
"""

image_store = {
    'panda': {
        'address': 'ledpi/images/panda.ppm',
        'description': 'A Panda shipped straight from China into our pi!',
        'offset': 11
    },
    'lion': {
        'address': 'ledpi/images/lion.ppm',
        'description': 'Ruler of the animal kingdom, watch out for scar!',
        'offset': 15
    },
    'eagle': {
        'address': 'ledpi/images/eagle.ppm',
        'description': 'Kawkaaaaw!',
        'offset': 14
    },
    'octopus': {
        'address': 'ledpi/images/octopus.ppm',
        'description': 'cpt. jack octopus',
        'offset': 16
    },
    'jellyfish': {
        'address': 'ledpi/images/jellyfish.ppm',
        'description': 'A fish made of jelly',
        'offset': 4
    },
    'tiger': {
        'address': 'ledpi/images/tiger.ppm',
        'description': 'From the himalayas to your heart',
        'offset': 10
    },
    'elephant': {
        'address': 'ledpi/images/elephant.ppm',
        'description': 'This one never forgets so be nice!',
        'offset': 15
    },
    'whale': {
        'address': 'ledpi/images/whale.ppm',
        'description': 'The biggest fish in the sea!',
        'offset': 11
    },
    'dragon': {
        'address': 'ledpi/images/dragon.ppm',
        'description': 'Just across the Wye!',
        'offset': 18
    },
    'turtle': {
        'address': 'ledpi/images/turtle.ppm',
        'description': 'Slow and steady wins the race',
        'offset': 11
    },
    'deep3': {
        'address': 'ledpi/images/deep3.ppm',
        'description': 'We\'re deep3, a secure by design software company!',
        'offset': 14
    },
    "calmoji": {
        'address': 'ledpi/images/calmoji.ppm',
        'description': 'does it need one?',
        'offset': 16
    }
}

def get_list_of_images():
    """
    converts our image dict into a list of image objects for ease of deserialisation for external services.
    NB. doesn't include the CALMOJI, this should not be exposed with the get all, it's a top secret deep3 image.
    :return: list of images.
    """
    image_list = []
    for key, value in image_store.items():
        if key is not 'calmoji':
            image_list.append(Image(key, value['description']))
    return image_list