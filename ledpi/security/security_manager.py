
class SecurityManger:
    """
    Manager object for handling api security note this is janked together to
    allow for some breadcrumbs.
    """

    @staticmethod
    def check_password(password):
        """
        Checks password and if wrong hints towards password location
        :param password: Password string.
        :return: Insecure error if fail true otherwise
        """

        try:
            if password == "piminister":
                return True
            else:
                return False
        except Exception as exc:
            return 'Error encountered checking password: [{0}]! the error is' \
                   '[{1}]'.format(password, exc)
