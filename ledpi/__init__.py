from flask import Flask
from ledpi.resources import image_resource, password_resource,\
    base64_decode_resource, team_progress_resource


def create_app(debug=False):
    """ Create the Application"""
    app = Flask(__name__)
    app.debug = debug
    app.register_blueprint(image_resource.route_blueprint)
    app.register_blueprint(password_resource.route_blueprint)
    app.register_blueprint(base64_decode_resource.route_blueprint)
    app.register_blueprint(team_progress_resource.route_blueprint)
    return app
