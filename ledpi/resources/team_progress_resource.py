import json
from flask import Blueprint
from ledpi.globals.global_vars import order_of_addition

route_blueprint = Blueprint('teams', __name__)


@route_blueprint.route('/teams/progress', methods=['GET'])
def get():
    """
    Method to return a list of encoded passwords.
    :return: list of base64 encoded passwords and http response.
    """

    if order_of_addition is []:
        return 'No teams have got their image on the board!', 200

    return json.dumps(order_of_addition), 200
