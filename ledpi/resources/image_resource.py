import json
from ledpi.ledcontrol.image_manager import display
from json.decoder import JSONDecodeError
from flask import Blueprint, request, Response
from ledpi.images.images import image_store, get_list_of_images
from ledpi.security.security_manager import SecurityManger
from ledpi.modles.message import Message

route_blueprint = Blueprint('register_image', __name__)

def obj_dict(obj):
    return obj.__dict__

@route_blueprint.route('/images', methods=['POST'])
def register_image():
    """
    Registers the image to be displayed on the led screen, should contain
    the name of the image eg {"name" : "<animal name>"}.
    :return: image name, a message and a http response code.
    """

    try:
        if SecurityManger.check_password(request.headers['password']) is False:
            return Response(Message('Password: [{0}] invalid'.format(
                request.headers['password'])), mimetype='application/json'), 403
        else:
            data = request.data.decode("utf-8")
            data = json.loads(data)
            image_to_display = image_store.get(data['name'])
            display(image_to_display, data['name'])

        return Response(Message('registered [{0}]'.format(data['name'])), mimetype='application/json'), 200
    except TypeError as error:
        return Response(Message('Error: [{0}]. Name value could not be found'\
            .format(error)), mimetype='application/json'), 404
    except JSONDecodeError as error:
        return Response(Message('Bad request: [{0}]'.format(error)), mimetype='application/json'), 400
    except KeyError as error:
        return Response(Message('Bad Request: Should be [{0}]'.format(error)), mimetype='application/json'), 400
    except Exception as error:
        return Response(Message('Error encountered: [{0}]'.format(error)), mimetype='application/json'), 500


@route_blueprint.route('/images/<image_name>', methods=['GET'])
def retrieve_image_details(image_name):
    """
    User requests information about an image, this will be stored in the
    image dict.
    :param image_name: name of the image to return info about.
    :return: image description.
    """

    try:
        if image_store.get(image_name) is None:
            return 'This animal doesn\'t exist!', 404

        image_details = image_store.get(image_name)
        return json.dumps(image_details), 200
    except Exception as error:
        return 'Error encountered: [{0}]'.format(error), 500


@route_blueprint.route('/images', methods=['GET'])
def retrieve_all():
    """
    Gets all image details.
    :return: list of all image details.
    """

    try:
        return Response(json.dumps(get_list_of_images(), default=obj_dict), mimetype='application/json'), 200
    except Exception as error:
        return Response(Message('Error encountered: [{0}]'.format(error)), mimetype='application/json'), 500
