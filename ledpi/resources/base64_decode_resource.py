import base64
import json
from flask import request, Blueprint
from json.decoder import JSONDecodeError

route_blueprint = Blueprint('base64_decoder', __name__)


@route_blueprint.route('/decode', methods=['POST'])
def get_decoded_password():
    """
    Returns a decoded password from a base64 input.
    :return: Decoded password and http response.
    """

    try:
        data = request.data.decode("utf-8")
        data = json.loads(data)

        return base64.b64decode(data['input']), 200
    except JSONDecodeError as error:
        return 'Bad request: [{0}]'.format(error.msg), 400
    except KeyError as error:
        return 'Bad Request: Should be [{0}]'.format(error), 400
    except Exception as error:
        return 'Internal Server Error: [{0}]'.format(error), 500
