import json
from flask import Blueprint, Response

route_blueprint = Blueprint('passwords', __name__)


@route_blueprint.route('/passwords', methods=['GET'])
def get():
    """
    Method to return a list of encoded passwords.
    :return: list of base64 encoded passwords and http response.
    """

    passwords = [
        'cHl0aG9u',  # python
        'TG9uZG9u',  # London
        'cGltaW5pc3Rlcg==',  # piminister
        'c2VjdXJpdHk='] # security
                   

    return Response(json.dumps(passwords), mimetype='application/json'), 200
