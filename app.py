from ledpi import create_app
from flask_cors import CORS, cross_origin

if __name__ == '__main__':
    app = create_app(debug=True)
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.run(host="0.0.0.0", port=8400)
